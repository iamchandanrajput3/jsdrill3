function values(obj) {

    if(typeof obj !== 'object' || obj === null || Array.isArray(obj))
    {
        throw new Error("Invalid arguments");
    }

    let valuesArr = [];
    for(let key in obj)
    {
        valuesArr.push(obj[key]);
    }
    return valuesArr;
}

export default values;