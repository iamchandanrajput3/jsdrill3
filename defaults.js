import _ from "underscore";
function defaults(obj, defaultProps) {

    if(typeof obj !== 'object' || typeof defaultProps !== 'object' || obj === null || defaultProps === null || Array.isArray(obj) || Array.isArray(defaultProps))
    {
        throw new Error("Invalid arguments");
    }

    let resultObj = {};
    resultObj = _.defaults(obj,defaultProps);
    return resultObj;
}

export default defaults;