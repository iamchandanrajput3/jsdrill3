function mapObject(obj, cb) {

    if(typeof obj !== 'object' || obj === null || Array.isArray(obj) || typeof cb !== 'function')
    {
        throw new Error("Invalid arguments");
    }

    let mapObj = cb(obj);
    return mapObj;
}

export default mapObject;