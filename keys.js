function keys(obj) {

    if(typeof obj !== 'object' || obj === null || Array.isArray(obj))
    {
        throw new Error("Invalid arguments");
    }

    let keysArr = [];
    for(let key in obj)
    {
        keysArr.push(key);
    }
    return keysArr;
}
export default keys;