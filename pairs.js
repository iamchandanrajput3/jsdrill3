function pairs(obj) {

    if(typeof obj !== 'object' || obj === null || Array.isArray(obj))
    {
        throw new Error("Invalid arguments");
    }

    let pairsArr = [];
    for(let key in obj)
    {
        pairsArr.push([key,obj[key]]);
    }
    return pairsArr;
}

export default pairs;