import invert from "../invert.js";

const obj = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
let invertedObj = {};

try{
    invertedObj = invert(obj);
} catch(e) {
    console.log(e);
}

console.log(invertedObj);