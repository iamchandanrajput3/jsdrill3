import values from "../values.js";

const obj = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
let valuesArr = [];

try{
    valuesArr = values(obj);
} catch(e) {
    console.log(e);
}

console.log(valuesArr);