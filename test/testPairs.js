import pairs from "../pairs.js";

const obj = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
let pairsArr = [];

try{
    pairsArr = pairs(obj);
} catch(e) {
    console.log(e);
}

console.log(pairsArr);