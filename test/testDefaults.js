import defaults from "../defaults.js";

const obj = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const defaultProps = {name:'Chandan', favoriteFood: 'Cake'};
let resultObj = {};

try{
    resultObj = defaults(obj,defaultProps);
} catch(e) {
    console.log(e);
}

console.log(resultObj);