import _ from "underscore";

function invert(obj) {

    if(typeof obj !== 'object' || obj === null || Array.isArray(obj))
    {
        throw new Error("Invalid arguments");
    }

    let invertedObj = {};
    invertedObj = _.invert(obj);
    return invertedObj;
}    

export default invert;